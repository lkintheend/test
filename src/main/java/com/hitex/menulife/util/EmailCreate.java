/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hitex.menulife.util;

import com.jscape.inet.email.EmailMessage;
import com.jscape.inet.smtpssl.SmtpSsl;
import java.util.List;

/**
 *
 * @author lkintheend
 */
public class EmailCreate {

    private String userName;
    private String password;
    private SmtpSsl smtp;
    private EmailMessage msg;

    public EmailCreate() {
//        this.userName = userName;
//        this.password = password;
        this.userName = "theanhkazu@gmail.com";
        this.password = "theanh311092";
        smtp = new SmtpSsl("smtp.gmail.com", 465);
    }

    public EmailCreate login() throws Exception {
        smtp.connect();
        smtp.login(userName, password);
        return this;
    }

    public void send(String to, String messageHeader, String messageBody) throws Exception {
        msg = new EmailMessage();
        msg.setFrom(userName);
        msg.setTo(to);
        msg.setContentType("text/plain", "UTF-8");
        msg.setSubject(messageHeader, "UTF-8");
        msg.setBody(messageBody, "UTF-8");
        smtp.send(msg);
    }

    public void send(List<String> listMail, String messageHeader, String messageBody) throws Exception {
        msg = new EmailMessage();
        msg.setFrom(userName);
        String to = listMail.get(0);
        StringBuilder bccBuilder = new StringBuilder();
        if (listMail.size() > 1) {
            for (int i = 1; i < listMail.size(); i++) {
                bccBuilder.append(listMail.get(i)).append(",");
            }
        }
        msg.setTo(to);
        msg.setBcc(bccBuilder.toString());
        msg.setContentType("text/plain", "UTF-8");
        msg.setSubject(messageHeader, "UTF-8");
        msg.setBody(messageBody, "UTF-8");
        smtp.send(msg);
    }

    public void send(String to, String bcc, String messageHeader, String messageBody) throws Exception {
        msg = new EmailMessage();
        msg.setFrom(userName);
        msg.setTo(to);
        msg.setBcc(bcc);
        msg.setContentType("text/html", "UTF-8");
        msg.setSubject(messageHeader, "UTF-8");
        msg.setBody(messageBody, "UTF-8");
        smtp.send(msg);
    }

    public void disconnect() throws Exception {
        smtp.disconnect();
    }

    public EmailMessage getEmailMessage() {
        return this.msg;
    }

    public void newEmail(String to, String rememberToken){
        EmailCreate emailCore = new EmailCreate();
        try {
            emailCore.login();
            emailCore.send(to, "Email xác nhận đăng kí tài khoản manulife!!", "Click Link: http://123.31.41.31:8080/manulife/api/Active/"+ rememberToken);
            emailCore.disconnect();
        } catch (Exception ex) {
//            Logger.getLogger(EmailCore.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void forgotPassMail(String to, String id){
        EmailCreate emailCore = new EmailCreate();
        try {
            emailCore.login();
            emailCore.send(to, "Click link de doi mk!!","Click Link: http://localhost:8080/api?token=" + StringEncrypt.getInstance().encrypt(id));
            emailCore.disconnect();
        } catch (Exception ex) {
//            Logger.getLogger(EmailCore.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void main(String[] args) {
//        EmailCore emailCore = new EmailCore("theanhkazu@gmail.com", "theanh311092");
//        try {
//            emailCore.login();
//            emailCore.send("lkintheend10@gmail.com", "hello", "Xin chao Huypv \n Toi Huy day");
//            emailCore.disconnect();
//        } catch (Exception ex) {
////            Logger.getLogger(EmailCore.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        EmailCreate emailCore = new EmailCreate("theanhkazu@gmail.com", "theanh311092");
//        emailCore.newEmail("doandinhlinh.kma@gmail.com", "123123wqewqeqw");
    }
}
