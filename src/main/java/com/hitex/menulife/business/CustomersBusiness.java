/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hitex.menulife.business;

import com.hitex.menulife.dao.CustomersDao;
import com.hitex.menulife.model.Customers;
import java.util.ArrayList;

/**
 *
 * @author lkintheend
 */
public class CustomersBusiness {
    
    public ArrayList<Customers> getAllCustomers(int offset, int limit){
        CustomersDao customersDao = new CustomersDao();
        return customersDao.getAllCustomers(offset, limit);
    }
    
    public Customers getCustomersById(int id) {
        CustomersDao customersDao = new CustomersDao();
        return customersDao.getCustomersById(id);
    }
    
    public int countRow() {
        CustomersDao customersDao = new CustomersDao();
        return customersDao.countRow();
    }
    
}
