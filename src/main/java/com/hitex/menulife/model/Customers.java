/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hitex.menulife.model;

import com.hitex.menulife.util.Util;

/**
 *
 * @author lkintheend
 */
public class Customers {
    String id;
    String name;
    String age;
    String phone;
    String gender;
    String address;
    String email;
    String yourJob;
    String inCome;
    String need;
    String customerNew;
    String status;
    String createdAt;
    String updatedAt;

    public Customers() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getYourJob() {
        return yourJob;
    }

    public void setYourJob(String yourJob) {
        this.yourJob = yourJob;
    }

    public String getInCome() {
        return inCome;
    }

    public void setInCome(String inCome) {
        this.inCome = inCome;
    }

    public String getNeed() {
        return need;
    }

    public void setNeed(String need) {
        this.need = need;
    }

    public String getCustomerNew() {
        return customerNew;
    }

    public void setCustomerNew(String customerNew) {
        this.customerNew = customerNew;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = Util.convertStringToTimestamp(createdAt);
    }

    public String getUpdatedAt() {
        return this.createdAt = updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = Util.convertStringToTimestamp(updatedAt);
    }

    @Override
    public String toString() {
        return "Customers{" + "id=" + id + ", name=" + name + ", age=" + age + ", phone=" + phone + ", gender=" + gender + ", address=" + address + ", email=" + email + ", yourJob=" + yourJob + ", inCome=" + inCome + ", need=" + need + ", customerNew=" + customerNew + ", status=" + status + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt + '}';
    }
    
    
}
