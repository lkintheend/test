/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hitex.menulife.dao;

import com.hitex.menulife.config.MySqlDao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author lkintheend
 */
public class ImageDao {

    PreparedStatement ptmt = null;
    ResultSet resultSet = null;

    public ImageDao() {

    }

    private Connection getConnection() throws SQLException {
        Connection conn;
        conn = MySqlDao.getInstance().getConnection();
        return conn;
    }

    public String saveImage() {
        Connection connection = null;
        String sql = "";

        try {
            connection = getConnection();
//            sql = "select status from members where remember_token = ?";
            sql = "update members set image = ?  where id = ?";
            ptmt = connection.prepareStatement(sql);
//            ptmt.setString(1, MD5Generator.md5(newPassWord));
//            ptmt.setString(2, email);
//            ptmt.setString(3, MD5Generator.md5(oldPassWord));
//            System.out.println(ptmt.toString());
//            if (ptmt.executeUpdate() == 1) {
//                return true;
//            } else {
//                return false;
//            }
        } catch (SQLException ex) {
//            Logger.getLogger(MemberDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return "";
    }
}
