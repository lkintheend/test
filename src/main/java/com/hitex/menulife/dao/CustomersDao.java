/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hitex.menulife.dao;

import com.hitex.menulife.config.MySqlDao;
import com.hitex.menulife.model.Customers;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author lkintheend
 */
public class CustomersDao {

    PreparedStatement ptmt = null;
    ResultSet resultSet = null;

    public CustomersDao() {

    }

    private Connection getConnection() throws SQLException {
        Connection conn;
        conn = MySqlDao.getInstance().getConnection();
        return conn;
    }

    public ArrayList<Customers> getAllCustomers(int offset, int limit) {
        Connection connection = null;
        ArrayList<Customers> listProducts = new ArrayList<>();
        try {
            String queryString = "SELECT * from customers limit ? offset ?";
            System.out.println("queryString = " + queryString);
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setInt(1, limit);
            ptmt.setInt(2, offset);
            System.out.println(ptmt.toString());
            resultSet = ptmt.executeQuery();
            while (resultSet != null && resultSet.next()) {
                Customers c = new Customers();
                c.setId(resultSet.getString("id"));
                c.setName(resultSet.getString("name"));
                c.setAge(resultSet.getString("age"));
                c.setPhone(resultSet.getString("phone"));
                c.setGender(resultSet.getString("gender"));
                c.setAddress(resultSet.getString("address"));
                c.setEmail(resultSet.getString("email"));
                c.setYourJob(resultSet.getString("your_job"));
                c.setInCome(resultSet.getString("income"));
                c.setNeed(resultSet.getString("need"));
                c.setCustomerNew(resultSet.getString("customer_new"));
                c.setStatus(resultSet.getString("status"));
                c.setUpdatedAt(resultSet.getString("updated_at"));
                c.setCreatedAt(resultSet.getString("created_at"));

                listProducts.add(c);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return listProducts;
    }

    public Customers getCustomersById(int id) {
        Connection connection = null;
        Customers c = new Customers();
        try {
            String queryString = "SELECT * from customers where id = ?";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setString(1, String.valueOf(id));
            resultSet = ptmt.executeQuery();
            while (resultSet.next()) {
                c.setId(resultSet.getString("id"));
                c.setName(resultSet.getString("name"));
                c.setAge(resultSet.getString("age"));
                c.setPhone(resultSet.getString("phone"));
                c.setGender(resultSet.getString("gender"));
                c.setAddress(resultSet.getString("address"));
                c.setEmail(resultSet.getString("email"));
                c.setYourJob(resultSet.getString("your_job"));
                c.setInCome(resultSet.getString("income"));
                c.setNeed(resultSet.getString("need"));
                c.setCustomerNew(resultSet.getString("customer_new"));
                c.setStatus(resultSet.getString("status"));
                c.setUpdatedAt(resultSet.getString("updated_at"));
                c.setCreatedAt(resultSet.getString("created_at"));
            }
            return c;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return c;
    }

//    public boolean insertCustomers() {
//        
//
//    public String insertMember(String name, String fullName, String passWord, String phone, String email) {
//        Connection connection = null;
//        Random rd = new Random();
//        Long rdString = rd.nextLong() + System.currentTimeMillis(); //ham tao String ngau nhien tao rememberToken
//        try {
//            connection = getConnection();
//            String sql = "insert into members(name, full_name,password, phone, email, status, remember_token) values (?, ?, ?, ?, ?, 0, ?) ";
//            ptmt = connection.prepareStatement(sql);
//            ptmt.setString(1, name);
//            ptmt.setString(2, fullName);
//            ptmt.setString(3, MD5Generator.md5(passWord));
//            ptmt.setString(4, phone);
//            ptmt.setString(5, email);
//            ptmt.setString(6, MD5Generator.md5(rdString.toString()));
//            System.out.println("ptmt = " + ptmt.toString());
//            ptmt.execute();
//            return MD5Generator.md5(rdString.toString());
//        } catch (SQLException e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                if (resultSet != null) {
//                    resultSet.close();
//                }
//                if (ptmt != null) {
//                    ptmt.close();
//                }
//                if (connection != null) {
//                    connection.close();
//                }
//            } catch (SQLException e) {
//                e.printStackTrace();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//        }
//        return "";
//    }

    public int countRow() {
        Connection connection = null;
        String tableCount = "customers";

        try {
            String queryString = "SELECT COUNT(id) FROM " + tableCount;
            System.out.println(queryString);
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            resultSet = ptmt.executeQuery();
            if (resultSet != null && resultSet.next()) {
                return resultSet.getInt(1);
            }
            return 0;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return 0;
    }

    public static void main(String[] args) {
        CustomersDao c = new CustomersDao();
        System.out.println(c.getCustomersById(8));
    }
}
