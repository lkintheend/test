/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hitex.menulife.service;

import com.google.gson.Gson;
import com.hitex.menulife.business.CheckBusiness;
import com.hitex.menulife.business.CustomersBusiness;
import com.hitex.menulife.business.ProductBusiness;
import com.hitex.menulife.dao.CustomersDao;
import com.hitex.menulife.model.Customers;
import com.hitex.menulife.model.Product;
import com.hitex.menulife.model.RequestData;
import com.hitex.menulife.model.ResponseData;
import com.hitex.menulife.model.ResponseDataPaging;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author lkintheend
 */
@Path("/")

public class CustomersService {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of NewsService
     */
    public CustomersService() {
    }

    @POST
    @Path("/getAllCustomers")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    public Response getAllProducts(InputStream incomingData) {
        try {

            ResponseDataPaging response = new ResponseDataPaging("1", "Không xác định", null, 0);
            CustomersBusiness customersBusiness = new CustomersBusiness();

            StringBuilder crunchifyBuilder = new StringBuilder();
            Gson gson = new Gson();

            int page, limit;

            try {
                BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
                String line = null;
                while ((line = in.readLine()) != null) {
                    crunchifyBuilder.append(line);
                }
            } catch (Exception e) {
                return Response.status(200).entity(new Gson().toJson(response)).build();
            }

            System.out.println("Data Received: " + crunchifyBuilder.toString());

            RequestData requestData = gson.fromJson(crunchifyBuilder.toString(), RequestData.class);
            org.json.JSONObject jsonObject = new org.json.JSONObject(new Gson().toJson(requestData.getWsRequest()));

            page = jsonObject.getInt("page");
            limit = jsonObject.getInt("limit");
            ArrayList<Customers> listCustomers = customersBusiness.getAllCustomers(page * limit, limit);
            int totalPage = (int) Math.ceil((float) customersBusiness.countRow() / limit);

            if (listCustomers.isEmpty()) {
                response = new ResponseDataPaging("1", "Loi phan trang", null, totalPage);
            } else {
                response = new ResponseDataPaging("0", "thanh cong", listCustomers, totalPage);
            }

            return Response.status(200).entity(new Gson().toJson(response)).build();
        } catch (Exception e) {
            e.printStackTrace();
            ResponseData response = new ResponseData("1", "Không xác định", null);
            return Response.status(200).entity(new Gson().toJson(response)).build();
        }
    }

    @POST
    @Path("/getCustomerById")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    public Response getCustomerById(InputStream incomingData) {
        try {
            StringBuilder crunchifyBuilder = new StringBuilder();
            ResponseData response = new ResponseData("1", "Không xác định", null);
            Gson gson = new Gson();
            CustomersBusiness customersBusiness = new CustomersBusiness();

            Customers customers;
            try {
                BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
                String line = null;
                while ((line = in.readLine()) != null) {
                    crunchifyBuilder.append(line);
                }
            } catch (Exception e) {

                return Response.status(200).entity(new Gson().toJson(response)).build();
            }
            System.out.println("Data Received: " + crunchifyBuilder.toString());

            RequestData requestData = gson.fromJson(crunchifyBuilder.toString(), RequestData.class);
            org.json.JSONObject jsonObject = new org.json.JSONObject(new Gson().toJson(requestData.getWsRequest()));

            customers = customersBusiness.getCustomersById(Integer.valueOf(jsonObject.getString("id")));
            if (customers.getId() != null) {
                response = new ResponseData("0", "thanh cong", customers);
            } else {
                response = new ResponseData("0", "khong tim thay id", null);
            }

            return Response.status(200).entity(new Gson().toJson(response)).build();
        } catch (Exception e) {
            e.printStackTrace();
            ResponseData response = new ResponseData("1", "Không xác định", null);
            return Response.status(200).entity(new Gson().toJson(response)).build();
        }
    }

    @POST
    @Path("/getCustomerCare")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    public Response getCustomerCare(InputStream incomingData) {
        try {
            StringBuilder crunchifyBuilder = new StringBuilder();
            ResponseData response = new ResponseData("1", "Không xác định", null);
            Gson gson = new Gson();
            CustomersBusiness customersBusiness = new CustomersBusiness();
            int page, limit;
            ArrayList<Customers> listCustomers = new ArrayList<>();
            try {
                BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
                String line = null;
                while ((line = in.readLine()) != null) {
                    crunchifyBuilder.append(line);
                }
            } catch (Exception e) {

                return Response.status(200).entity(new Gson().toJson(response)).build();
            }
            System.out.println("Data Received: " + crunchifyBuilder.toString());

            RequestData requestData = gson.fromJson(crunchifyBuilder.toString(), RequestData.class);
            org.json.JSONObject jsonObject = new org.json.JSONObject(new Gson().toJson(requestData.getWsRequest()));

            page = jsonObject.getInt("page");
            limit = jsonObject.getInt("limit");

            listCustomers = customersBusiness.getAllCustomers(page * limit, limit);
            int totalPage = (int) Math.ceil((float) customersBusiness.countRow() / limit);
            if (listCustomers.isEmpty()) {
                    response = new ResponseDataPaging("1", "Loi phan trang", null, totalPage);
                } else {
                    response = new ResponseDataPaging("0", "thanh cong", listCustomers, totalPage);
                }

            return Response.status(200).entity(new Gson().toJson(response)).build();
        } catch (Exception e) {
            e.printStackTrace();
            ResponseData response = new ResponseData("1", "Không xác định", null);
            return Response.status(200).entity(new Gson().toJson(response)).build();
        }
    }
}
