/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hitex.menulife.service;

/**
 *
 * @author lkintheend
 */
import com.hitex.menulife.business.MemberBusiness;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

@Path("/Active")
public class ActiveMemberService {

    @GET
    @Path("/{token}")
    public Response Active(
            @PathParam("token") String token
    ) {

        MemberBusiness membersBI = new MemberBusiness();
        if (membersBI.activeMember(token)) {
            return Response.status(200)
                .entity("Actived")
                .build();
        } else {
            return Response.status(200)
                .entity("Cant Actived")
                .build();
        }

    }
}
