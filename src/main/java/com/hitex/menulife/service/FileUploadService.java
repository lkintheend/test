package com.hitex.menulife.service;

import com.google.gson.Gson;
import com.hitex.menulife.model.RequestData;
import com.hitex.menulife.model.ResponseData;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import sun.misc.BASE64Decoder;

//import org.glassfish.jersey.media.multipart.FormDataParam;
@Path("/")
public class FileUploadService {

    @Context
    private UriInfo context;

    public FileUploadService() {

    }

    @POST
    @Path("/upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadFile(
            @FormDataParam("file") InputStream uploadedInputStream,
            @FormDataParam("file") FormDataContentDisposition fileDetail) {
//        String fileLocation = "/home/lkintheend/WORK_DDL/" + fileDetail.getFileName();
        String fileLocation = "/home/manulife/public_html/public/upload/img/a.jpg";
        //saving file  
        System.out.println("filetype" + fileDetail.getType() + "" + fileDetail.getFileName());
        try {
            FileOutputStream out = new FileOutputStream(new File(fileLocation));
            int read = 0;
            byte[] bytes = new byte[1024];
            out = new FileOutputStream(new File(fileLocation));
            while ((read = uploadedInputStream.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            String output = "File successfully uploaded to : " + fileLocation;

            out.flush();
            out.close();
            return Response.status(200).entity(output).build();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Response.status(200).entity(null).build();
    }

    @POST
    @Path("/pto_getImage")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    public Response getImage(InputStream incomingData) {

        try {
            StringBuilder crunchifyBuilder = new StringBuilder();
            try {
                BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
                String line = null;
                while ((line = in.readLine()) != null) {
                    crunchifyBuilder.append(line);
                }
            } catch (Exception e) {
                System.out.println("Error Parsing: - ");
                return Response.status(200).entity("a").build();
            }
            String fileRepository = "/home/manulife/public_html/public/upload/img/";
            System.out.println("Data Received: " + crunchifyBuilder.toString());
            Gson gson = new Gson();
            RequestData requestData = gson.fromJson(crunchifyBuilder.toString(), RequestData.class);
            org.json.JSONObject jsonObject = new org.json.JSONObject(new Gson().toJson(requestData.getWsRequest()));
            ResponseData response = new ResponseData("1", " Chưa truyền tham số file_data", null);
            String nameStr = System.currentTimeMillis() + ".";
            if (jsonObject.has("file_data")) {

                String file_data = jsonObject.getString("file_data");
                String[] arr = file_data.split(",");
                String extension = arr[0].split(";")[0].split("/")[1];
                saveToFile(arr[1], fileRepository + nameStr + extension, extension);
                response = new ResponseData("0", fileRepository + nameStr + extension, nameStr + extension);
                System.out.println(response);
            }
            return Response.status(200).entity(gson.toJson(response)).build();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("------------------------- ");
            System.out.println(e);
            return Response.status(200).entity("Loi").build();
        }
    }

    private void saveToFile(String data, String target, String file_ext) {

        try {
            OutputStream out = null;
            BufferedImage image = null;
            byte[] imageByte;
            BASE64Decoder decoder = new BASE64Decoder();
            imageByte = decoder.decodeBuffer(data);
            Files.write(Paths.get(target), imageByte);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
